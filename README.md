# CookiePage

CookiePage is a Pale Moon extensions to show all cookies belonging to the current page.

#### Supports
 * Pale Moon [33.0 - 33.*]

## Building
Simply download the contents of the repository and pack the src folder into a .zip file. Then, rename the file to .xpi and drag into the browser.

## Download
You can grab the latest release from the [Official Web Site](//realityripple.com/Software/XUL/CookiePage/).
